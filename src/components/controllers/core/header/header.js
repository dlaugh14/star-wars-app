/**
 * Created by laugh on 7/14/2018.
 */
import React, { Component } from 'react';
import VolumeOff from 'material-ui-icons/VolumeOff';
import VolumeUp from 'material-ui-icons/VolumeUp';

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {

    const styles = {

      largeIcon: {
        width: 50,
        height: 50,
      }

    };
    let soundBtn = ''

    if (this.props.play && this.props.play !== '' && (this.props.play.getPlayerState() === 5 || this.props.play.getPlayerState() === -1)) {
      soundBtn = ''
    } else {
      soundBtn = <div>
      {this.props.volume === false &&
      <VolumeOff style={styles.largeIcon} onClick={this.props.updateVolume.bind(this)} color="white"></VolumeOff>
      }
      {this.props.volume === true &&
      <VolumeUp style={styles.largeIcon} onClick={this.props.updateVolume.bind(this)} color="white"></VolumeUp>
      }
      </div>
    }

    return (
      <header id="header" >
        <div className="logo">
          <span className="icon fa-diamond"></span>
        </div>
        <div className="content">
          <div className="inner">
            <h1>Star Wars</h1>
            <p>Choose your favorite star wars film and get character facts.</p>
            {soundBtn}
          </div>
        </div>
      </header>
    )
  }
}

export default Header;

/**
 * Created by laugh on 7/14/2018.
 */
import React, { Component } from 'react';
import './../../../../App.scss';
import BackgroundVideo from '../../../views/background-video';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CharacterDialog from '../../../views/character-dialog';
import classNames from 'classnames';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import gql from "graphql-tag";
import Grid from '@material-ui/core/Grid';
import Header from '../../core/header/header';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import {setFilms} from '../../../../actions/actions';

import { execute } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';

const styles = {

  root: {
    background: 'transparent',
    '&:hover': {
      background: 'rgba(0, 0, 0, 0.5)',
    },
  },
  expandRoot: {
    background: 'rgba(255,255,255, 0.7)',
    '&$hover': {
      background: 'white',
    },
  }
}

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {films: '', play: '', opts: '', volume: false, open: false, activeCharacter: '', retryTimes: 0, activeCharacterWorld: ''};
        this._onEnd = this._onEnd.bind(this);
        this._onReady = this._onReady.bind(this);
        this.updateVolume = this.updateVolume.bind(this);
        this.Transition = this.Transition.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.characterSort = this.characterSort.bind(this);
        this.retry = this.retry.bind(this);
    }

  retry() {
    const globalProps = this.props;

    const link = new HttpLink({
      uri: process.env.REACT_APP_API_HOST
    });

    const operation = {
      query: gql`
    query{allFilms(after:"A New Hope") {
      edges {
        node {
          id,
          title,
          episodeID,
          openingCrawl,
          director,
          producers,
          releaseDate,
          characterConnection{
            characters{
              name,
              birthYear,
              eyeColor,
              gender,
              hairColor,
              height,
              mass,
              skinColor,
              homeworld {
                id,
                name
              }
            }
          }
        }
      }
     } 
    }
    `,
    };

    execute(link, operation).subscribe({
      next: result => {
        globalProps.setFilms(result.data.allFilms);
        this.setState(prevState => ({
          films: result.data.allFilms.edges
        }));
      },
      error: error => {
        this.setState(prevState => ({
          retryTimes: this.state.retryTimes + 1
        }));
      },
      complete: () => {},
    });
  }

  componentWillMount() {
      if (!this.opts) {
        let opts = {
          playerVars: {
            autoplay: 1,
            controls: 0,
            rel: 0,
            showinfo: 0,
            mute: 1
          }
        };
        this.setState(prevState => ({
          opts: opts
        }))
      }

    const globalProps = this.props;

    const link = new HttpLink({
      uri: process.env.REACT_APP_API_HOST
    });

    const operation = {
      query: gql`
    query{allFilms(after:"A New Hope") {
      edges {
        node {
          id,
          title,
          episodeID,
          openingCrawl,
          director,
          producers,
          releaseDate,
          characterConnection{
            characters{
              name,
              birthYear,
              eyeColor,
              gender,
              hairColor,
              height,
              mass,
              skinColor,
              homeworld {
                id,
                name
              }
            }
          }
        }
      }
     } 
    }
    `,
    };

      execute(link, operation).subscribe({
        next: result => {
          globalProps.setFilms(result.data.allFilms);
          this.setState(prevState => ({
            films: result.data.allFilms.edges
          }));
        },
        error: error => {
          this.setState(prevState => ({
            retryTimes: this.state.retryTimes + 1
          }));
        },
        complete: () => {},
      });
    }

  characterSort(characters) {
      let newCharacters = characters;
      newCharacters = newCharacters.slice().sort(function(a, b){
        if (typeof(a.name) === 'string') {
          var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
          if (nameA < nameB) //sort string ascending
            return -1
          if (nameA > nameB)
            return 1
          return 0 //default return value (no sorting)
        }
        return 0
      })
      return newCharacters;
  }

  updateVolume() {
      if(this.state.play !== '') {
        if (this.state.play.isMuted()) {
          this.state.play.unMute();
          this.state.play.setVolume(100);
        } else {
          this.state.play.mute();
        }
        this.setState(prevState => ({
          volume: !this.state.volume
        }));
      }
  }

  _onEnd(event) {
    this.state.play.playVideo();
  }

  _onReady(event) {
        this.setState({
          play: event.target,
        });
  }

  Transition(props) {
    return <Slide direction="up" {...props} />;
  }

  handleClickOpen = (character) => {
    this.setState({ activeCharacter: character });
    this.setState({ activeCharacterWorld: character.homeworld.name });
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    if (this.state.films === '' && this.state.retryTimes > 0 && this.state.retryTimes < 3) {
      this.retry()
    }
    const { classes } = this.props;

    return (
      <div>

        <Header play={this.state.play} updateVolume={this.updateVolume} volume={this.state.volume}/>

        {this.state.films !== '' &&
        <Grid container spacing={24} style={{padding: 24, color:'white'}}>
          { this.state.films.map((item, index) => (
            <Grid key={index} item xs={12} sm={6} lg={4} xl={3}>
              <Card className={classNames(classes.root)}>
                <CardContent>
                  <Typography style={{color: 'white'}} variant="headline" component="h2">
                    {item.node.title}
                  </Typography>
                  <Typography style={{color: 'white'}} color="textSecondary">
                    Directed By: {item.node.director}
                  </Typography>
                  <Typography style={{color: 'white'}} component="p">
                    Episode {item.node.episodeID}: released on {item.node.releaseDate}
                  </Typography>
                </CardContent>
                <ExpansionPanel className={classNames(classes.expandRoot)}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>{item.node.title} - Characters</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails style={{margin: '0', padding: 0}}>
                    <List component="nav" style={{width: '100%'}}>
                    { this.characterSort(item.node.characterConnection.characters).map((character, characterIndex) => {
                     return <ListItem key={'character' + characterIndex} button>
                         <ListItemText onClick={this.handleClickOpen.bind(this, character)} inset primary={character.name} />
                       </ListItem>
                    })
                    }
                    </List>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Card>
            </Grid>
          ))}
        </Grid>
        }
      <CharacterDialog closeDialog={this.handleClose} activeCharacter={this.state.activeCharacter} activeCharacterWorld={this.state.activeCharacterWorld} open={this.state.open}/>
      <BackgroundVideo onReady={this._onReady} onEnd={this._onEnd} opts={this.state.opts} play={this.state.play}/>
      </div>
    );
  }

}

const mapDispatchToProps = {
  setFilms,
};

const mapStateToProps = state => {
  return {
    session: state,
  }
};

export default withStyles(styles)(connect(
  mapStateToProps,
  mapDispatchToProps
)(Home));

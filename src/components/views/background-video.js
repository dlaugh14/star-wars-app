/**
 * Created by laugh on 7/15/2018.
 */
import React from 'react';
import YouTube from 'react-youtube';

import { withStyles } from '@material-ui/core/styles';

const styles = {

  root: {

  }
}

const BackgroundVideo = (props) => (
  <div className="video-background">
    <div className="video-foreground">
      {props.play && props.play !== '' && (props.play.getPlayerState() === 5 || props.play.getPlayerState() === -1) &&
        <div id="bg-overlay">
        </div>
      }
      <YouTube
        videoId="zB4I68XVPzQ"
        opts={props.opts}
        className="video-iframe"
        onReady={props.onReady}
        onEnd={props.onEnd}
      />
    </div>
  </div>
);

export default withStyles(styles)(BackgroundVideo);
/**
 * Created by laugh on 7/15/2018.
 */
import AppBar from '@material-ui/core/AppBar';
import CloseIcon from 'material-ui-icons/Close';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = {

  root: {
    background: 'transparent',
    '&:hover': {
      background: 'rgba(0, 0, 0, 0.5)',
    },
  },
  hover: {background: 'blue'},
}

const CharacterDialog = (props) => (
    <Dialog
        fullScreen
        open={props.open}
        onClose={props.closeDialog}
        TransitionComponent={this.Transition}
      >
    <AppBar style={{background: '#fd0000'}}>
    <Toolbar>
      <IconButton color="inherit" onClick={props.closeDialog.bind(this)} aria-label="Close">
        <CloseIcon />
        </IconButton>
        <Typography variant="title" color="inherit">
          Character
      </Typography>
    </Toolbar>
    </AppBar>
    <List>
    <ListItem button>
    <Typography style={{color: 'black', marginTop: '8.25%'} } variant="display2" align="center">
      {props.activeCharacter.name}
    </Typography>
    </ListItem>
    <Divider />
      <List>
        <ListItem>
          <Typography style={{color: 'black'} } variant="display1" align="center">
            {'Homeworld: ' + props.activeCharacterWorld}
          </Typography>
          </ListItem>
          <ListItem>
          <ListItemText primary="Birth Year" secondary={props.activeCharacter.birthYear} />
          </ListItem>
          <ListItem>
          <ListItemText primary="Eye Color" secondary={props.activeCharacter.eyeColor} />
          </ListItem>
          <ListItem>
          <ListItemText primary="Gender" secondary={props.activeCharacter.gender} />
          </ListItem>
          <ListItem>
          <ListItemText primary="Hair Color" secondary={props.activeCharacter.hairColor} />
          </ListItem>
          <ListItem>
          <ListItemText primary="Height" secondary={props.activeCharacter.height} />
          </ListItem>
          <ListItem>
          <ListItemText primary="Mass" secondary={props.activeCharacter.mass} />
          </ListItem>
          <ListItem>
          <ListItemText primary="Skin Color" secondary={props.activeCharacter.skinColor} />
          </ListItem>
          <Divider />
        </List>
        </List>
    </Dialog>
);

export default withStyles(styles)(CharacterDialog);
/**
 * Created by laugh on 9/21/2017.
 */
import {combineReducers} from 'redux';
import Films from './all-films';
/*
 * We combine all reducers into a single object before updated data is dispatched (sent) to store
 * Your entire applications state (store) is just whatever gets returned from all your reducers
 * */

const allReducers = combineReducers({
    results: Films,
});

export default allReducers
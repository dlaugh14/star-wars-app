import React, { Component } from 'react';
import './App.css';
import Home from './components/controllers/core/main/home';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

class App extends Component {
  render() {

    const client = new ApolloClient({
      uri: "http://localhost:51327"
    });

    return (
      <ApolloProvider client={client}>
        <MuiThemeProvider>
          <Home/>
        </ MuiThemeProvider>
      </ApolloProvider>
    );
  }
}

export default App;

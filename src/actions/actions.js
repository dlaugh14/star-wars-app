/**
 * Created by laugh on 7/25/2018.
 */
// actions.js
import types from './types.js'

export function setFilms (values) {
  return function (dispatch) {
    dispatch({ type: types.SET_FILMS, films: values })
  }

}

export default {
  setFilms
}
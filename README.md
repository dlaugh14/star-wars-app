This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find the most recent version of the create-react-app README [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Star Wars App

### Installation Instructions

1. Run `yarn install`).
2. Change .env.development file to url from .env.production file - OR skip to step 3.
3. For local development download and install [`swamp-graphql`](https://github.com/graphql/swapi-graphql) a wrapper for the Star Wars API OR and run `npm install` then `npm start`. Copy the url with port from terminal and replace url in .env.development file at the root of the project.
3. Run `npm start` and Star Wars App will open with http://localhost:3000/

### Demo
See live working demo at [`Dlaugh.com`](https://star-wars-app.dlaugh.com)